docker-up:
	docker start some-postgis my_pgadmin4
.PHONY: docker-up

docker-stop:
	docker stop some-postgis my_pgadmin4
.PHONY: docker-up

vscode:
	code .
.PHONY: vscode

qgis-start:
	qgis > /dev/null 2>&1 &
.PHONY: qgis-start

firefox:
	firefox -new-window --kiosk https://framagit.org/pole-si-cen38/observatoire_biodiv
.PHONY: firefox

run_script:
	scripts/com2zonages.sh
.PHONY: run_script

start:
	$(MAKE) docker-up
	$(MAKE) qgis-start
	$(MAKE) vscode
	$(MAKE) firefox
.PHONY: start

stop:
	$(MAKE) docker-down
	$(MAKE) qgis-start
	$(MAKE) vscode
.PHONY: start

#---VARIABLES
#---DOCKER---#
DOCKER = docker
DOCKER_RUN = $(DOCKER) run
DOCKER_COMPOSE= docker compose
DOCKER_COMPOSE_UP = $(DOCKER_COMPOSE) up -d
DOCKER_COMPOSE_STOP = $(DOCKER_COMPOSE) stop
#----#

# docker run --name some-postgis -e POSTGRES_PASSWORD=1234 -d -p 5415:5432 postgis/postgis
# docker run -it --rm -v /home/mich/Documents/gestionnaire2rte:/root/mydata/ osgeo/gdal:latest /bin/bash