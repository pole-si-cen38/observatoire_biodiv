-- Fait le ménage dans les schémas
DROP SCHEMA IF EXISTS communes CASCADE;
DROP SCHEMA IF EXISTS zonages CASCADE;
DROP SCHEMA IF EXISTS com_traitements CASCADE;
CREATE SCHEMA communes;
CREATE SCHEMA zonages;
CREATE SCHEMA com_traitements;


COMMENT ON SCHEMA communes IS 'Ensemble des données communale
Ign_communes';

COMMENT ON SCHEMA zonages IS 'Ensemble des données "zonages" récupérer via flux ou couches nationales
A COMPLETER
Parcs_Naturels_regionaux
Parcs_Nationaux
Znieff2
Znieff1
Arretes_de_protection_de_biotope
Reserves_naturelles_nationales
Reserves_naturelles_regionales
Reserves_nationales_de_chasse_et_faune_sauvage
Reserve_biologique
Forets_publique
Parcelles_forets_publique
ZICO
N2000_ZPS
N2000_ZSC
--------ISERE-----
zones_humides
Pelouses sèches
 ';