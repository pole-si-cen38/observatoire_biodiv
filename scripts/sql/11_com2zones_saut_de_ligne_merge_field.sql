--SELECT ST_SRID(geom) FROM com_traitements.arretes_de_protection_de_biotope LIMIT 1;-- 900914
--SELECT ST_SRID(geom) FROM com_traitements.cen_mu LIMIT 1;--900914
--SELECT ST_SRID(geom) FROM com_traitements.epci LIMIT 1;--900915
--SELECT ST_SRID(geom) FROM com_traitements.n2000_sic LIMIT 1;--900914
--SELECT ST_SRID(geom) FROM com_traitements.zones_humides_isere LIMIT 1;--900914

DROP TABLE IF EXISTS com_traitements.communes_intersects_zonages_aggreg_saut_ligne_merge_field_cen38;
CREATE TABLE com_traitements.communes_intersects_zonages_aggreg_saut_ligne_merge_field_cen38 as
(
with
		pnr as
		(
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.pnr , chr(10)) as pnr
				FROM

					(
					SELECT a."a.insee_com",	concat(b.id, ' : ', b.nom) as pnr,
					(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
					FROM communes.ign_communes_38 a, zonages.parcs_naturels_regionaux b
					WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
					--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
					order by percent_recouvrement--a."a.insee_com"
					)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		 ),
		 pn as
		 (
SELECT	b."a.insee_com" as insee_com,
				string_agg(b.pn , chr(10)) as pn
				FROM

						(
						SELECT a."a.insee_com",	concat(b.id,' : ', b.nom) as pn,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.parcs_nationaux b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		 ),
		 znieff2 as -- peut être changer la valeur d'intersection 1% semble trop resctrictif (05/05/2023)
		 (
SELECT	b."a.insee_com" as insee_com,
						string_agg(b.znieff2 , chr(10)) as znieff2
						
				FROM

						(
						SELECT a."a.insee_com",	concat(b.id, ' : ', b.nom) as znieff2,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.znieff2 b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com" 
		 ) ,
		Znieff1 as
		 (
SELECT	b."a.insee_com" as insee_com,
						string_agg(b.znieff1 , chr(10)) as znieff1
						
				FROM

						(
						SELECT a."a.insee_com",	concat(b.id, ' : ', b.nom) as znieff1,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.znieff1 b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						--AND (st_area(st_intersection(a.geom, d.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com" 
			 ),
		appb as
		 (
SELECT	b."a.insee_com" as insee_com,
						string_agg(b.appb , chr(10)) as appb
						
				FROM

						(
						SELECT a."a.insee_com", concat(	b.id,' : ', b.nom) as appb,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.arretes_de_protection_de_biotope b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com" 
		 ) ,
		rnn as
		 (
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.reserves_naturelles_nationales , chr(10)) as reserves_naturelles_nationales
						
				FROM

						(
						SELECT a."a.insee_com", concat(	b.id,' : ', b.nom) as reserves_naturelles_nationales,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.reserves_naturelles_nationales b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"	 
		 ),
		rnr as
		 (
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.reserves_naturelles_regionales, chr(10)) as reserves_naturelles_regionales
				FROM		
						(
						SELECT a."a.insee_com", concat(	b.id,' : ', b.nom) as reserves_naturelles_regionales,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.reserves_naturelles_regionales b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com" 
		 ),
		 Reserve_biologique as ------------------CONTINUER
		 (
SELECT	b."a.insee_com" as insee_com,
						string_agg(b.reserve_biologique , chr(10)) as reserve_biologique
				FROM

						(
						SELECT a."a.insee_com",	concat(b.id,' : ', b.nom) as reserve_biologique,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.reserve_biologique b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com" 
		 ),
		 ens_zo1 as
		 (
SELECT	b."a.insee_com" as insee_com,
						string_agg(b.ens_zo_01 , chr(10)) as ens_zo_01
				FROM

						(
						SELECT a."a.insee_com",	concat(b.id,' : ', b.nom) as ens_zo_01,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.ens_zo_01 b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"	
		 ),
		 ens_zo2 as
		 (
SELECT	b."a.insee_com" as insee_com,
						string_agg(b.ens_zo_02 , chr(10)) as ens_zo_02
				FROM

						(
						SELECT a."a.insee_com",	concat(b.id,' : ', b.nom) as ens_zo_02,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.ens_zo_02 b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"	
		 ),
		zico as
		(	
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.zico , chr(10)) as zico
				FROM

						(
						SELECT a."a.insee_com",	concat(b.id,' : ', b.nom) as zico,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.zico b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		),
			n2000_sic as
		(	
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.n2000_sic , chr(10)) as n2000_sic
				FROM

						(
						SELECT a."a.insee_com",	concat(b.id,' : ', b.nom) as n2000_sic,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.n2000_sic b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		),
			n2000_zps as
		(	
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.n2000_zps , chr(10)) as n2000_zps
				FROM

						(
						SELECT a."a.insee_com",	concat(b.id,' : ', b.nom) as n2000_zps,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.n2000_zps b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		),
			zh as
		(	
SELECT	b."a.insee_com" as insee_com,
		string_agg(b.zones_humides , chr(10)) as zones_humides
FROM
		
		(
		SELECT a."a.insee_com",	concat(b.site_code, ' : ',b.site_name) as zones_humides,
		(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
		FROM communes.ign_communes_38 a, zonages.zones_humides_isere_corect_topo b
		WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
		--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
		order by percent_recouvrement--a."a.insee_com"
		)b
		
GROUP BY b."a.insee_com"
order by b."a.insee_com"
		),
			ps as
		(	
		SELECT	b."a.insee_com" as insee_com,
		string_agg(b.pelouses_seches::text , chr(10)) as pelouses_seches
FROM
		
		(
		SELECT a."a.insee_com",	concat(b.ogc_fid,' : ') as pelouses_seches,
		(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
		FROM communes.ign_communes_38 a, zonages.pelouses_seches_corect_topo b
		WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
		--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
		order by percent_recouvrement--a."a.insee_com"
		)b
		
GROUP BY b."a.insee_com"
order by b."a.insee_com"
		),
			rncfs as
		(	
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.reserves_nationales_de_chasse_et_faune_sauvage , chr(10)) as reserves_nationales_de_chasse_et_faune_sauvage
				FROM

						(
						SELECT a."a.insee_com",	concat(b.id,' : ', b.nom) as reserves_nationales_de_chasse_et_faune_sauvage,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.reserves_nationales_de_chasse_et_faune_sauvage b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		),
		onf_forets_publique as
		(	
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.forets_publique , chr(10)) as forets_publique
				FROM

						(
						SELECT a."a.insee_com", concat(b.id,' : ', b.nom) as forets_publique ,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.forets_publique b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		),
	
		Parcelles_forets_publique as 		
		(	
SELECT	b."a.insee_com" as insee_com,
		string_agg(b.parcelles_forets_publique::text , chr(10)) as parcelles_forets_publique
FROM
		
		(
		SELECT a."a.insee_com",	concat(b.id ,' : ', b.nom) as parcelles_forets_publique,
		(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
		FROM communes.ign_communes_38 a, zonages.parcelles_forets_publique b
		WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
		--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
		order by percent_recouvrement--a."a.insee_com"
		)b
		
GROUP BY b."a.insee_com"
order by b."a.insee_com"
		),
		srce_rb as
		(	
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.srce_reservoir_biodiv , chr(10)) as srce_reservoir_biodiv,
						'' as nom,
						'' as gest_site
				FROM
						
						(
						SELECT a."a.insee_com",	concat(b.id_resv,' : ') as srce_reservoir_biodiv,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.srce_reservoir_biodiv_corect_topo b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b
						
				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		),
	inv_cours_eau_police_eau as
	(
	SELECT	b."a.insee_com" as insee_com,
		string_agg(b.inventaire_cours_eau_police_eau::text , chr(10)) as inventaire_cours_eau_police_eau
FROM
		
		(
		SELECT a."a.insee_com", concat (b.id_loc , ' : ', b.nom) as inventaire_cours_eau_police_eau ,-- b.nom, b.gest_site,
			st_length(b.geom) as longeur_segment,
		(st_length(st_intersection(a.geom, b.geom)) / st_length(b.geom))*100 as percent_recouvrement
		FROM communes.ign_communes_38 a, zonages.inventaire_cours_eau_police_eau b
		WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
		--AND (st_length(st_intersection(a.geom, b.geom)) / st_length(b.geom))*100 > 1
		order by percent_recouvrement--a."a.insee_com"
		)b
		
GROUP BY b."a.insee_com"
order by b."a.insee_com"
	)
	


SELECT
a."a.insee_com" as insee_com,
b.pnr, --id as pnr_id, b.nom as pnr_nom, -- PNR
c.pn, --id as pn_id, c.nom as pn_nom, -- PN
d.znieff2, --id as znieff2_id, d.nom as znieff2_nom, -- znieff2
e.znieff1, --id as znieff1_id, e.nom znieff1_nom, -- znieff1
f.appb, --id as appb_id, f.nom as appb_nom, -- appb
g.reserves_naturelles_nationales, --id as rnn_id, g.nom as rnn_nom , -- rnn
h.reserves_naturelles_regionales, --id as rnr_id, h.nom as rnr_nom, -- rnr
i.reserve_biologique, --id as rb_id, i.nom as rb_nom, -- Reserve biologique
k.ens_zo_01, --id as ens_z01_id, k.nom as ens_z01_nom, -- ENS_ZO1
l.ens_zo_02, --id as ens_z02_id, l.nom as ens_z02_nom, --ENS_ZO2
m.zico, --id as zico_id, m.nom as zico_nom, --ZICO
n.n2000_sic, --id as n2000_sic_id , n.nom as n2000_sic_nom, --n2000 sic
o.n2000_zps, --id as n2000_zps_id, o.nom as n2000_zps_nom, -- n2000_zps
p.zones_humides, --id as zh_id,-- p.nom as zh_site_nom, -- zh
q.pelouses_seches, --id as ps_id, --q.nom as ps_nom, -- ps
r.reserves_nationales_de_chasse_et_faune_sauvage, --id as rncfs_id, r.nom as rncfs_nom, -- RNCFS
t.forets_publique, --id as foret_pub_id, t.nom as foret_pub_nom, -- Foret publique
u.parcelles_forets_publique, --id as parcel_foret_pub_id, u.nom as parcel_foret_pub_nom, -- Parcelle foret publique
v.srce_reservoir_biodiv, --id as srce_reservoir_biodiv, 
w.inventaire_cours_eau_police_eau, --id as inv_cours_eau_police_eau,
a.geom

FROM communes.ign_communes_38 a
LEFT JOIN pnr b ON a."a.insee_com" = b.insee_com
LEFT JOIN pn c ON a."a.insee_com" = c.insee_com
LEFT JOIN znieff2 d ON a."a.insee_com" = d.insee_com
LEFT JOIN znieff1 e ON a."a.insee_com" = e.insee_com
LEFT JOIN appb f ON a."a.insee_com" = f.insee_com
LEFT JOIN rnn g ON a."a.insee_com" = g.insee_com
LEFT JOIN rnr h ON a."a.insee_com" = h.insee_com
LEFT JOIN Reserve_biologique i ON a."a.insee_com" = i.insee_com
LEFT JOIN ens_zo1 k ON a."a.insee_com" = k.insee_com
LEFT JOIN ens_zo2 l ON a."a.insee_com" = l.insee_com
LEFT JOIN zico m ON a."a.insee_com" = m.insee_com
LEFT JOIN n2000_sic n ON a."a.insee_com" = n.insee_com
LEFT JOIN n2000_zps o ON a."a.insee_com" = o.insee_com
LEFT JOIN zh p ON a."a.insee_com" = p.insee_com
LEFT JOIN ps q ON a."a.insee_com" = q.insee_com
LEFT JOIN rncfs r ON a."a.insee_com" = r.insee_com
LEFT JOIN onf_forets_publique t ON a."a.insee_com" = t.insee_com
LEFT JOIN Parcelles_forets_publique u ON a."a.insee_com" = u.insee_com
LEFT JOIN srce_rb v ON a."a.insee_com" = v.insee_com
LEFT JOIN inv_cours_eau_police_eau w ON a."a.insee_com" = w.insee_com


);