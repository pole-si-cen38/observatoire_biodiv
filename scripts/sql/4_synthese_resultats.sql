DROP TABLE IF EXISTS com_traitements.comm_synthese_intersects_cen38;
CREATE TABLE com_traitements.comm_synthese_intersects_cen38 as

(
SELECT
a."a.insee_com",
concat_ws(';', a.pnr,a.pn,a.znieff1,a.znieff2, a.appb, a.rnn, a.rnr,
		  a.rb, a.ens_z01, a.ens_z02, a.zico, a.n2000_sic, a.n2000_zps,
		  a.rncfs, a.foret_pub) as synthese,
a.geom
FROM
(
SELECT
"a.insee_com",d
CASE WHEN pnr_nom is NOT NULL THEN concat('pnr : ', pnr_nom) END AS pnr,
CASE WHEN pn_nom is NOT NULL THEN concat('pn : ', pn_nom) END AS pn,
CASE WHEN znieff1_id is NOT NULL THEN concat('znieff1_id : ', znieff1_id) END AS znieff1,
CASE WHEN znieff2_id is NOT NULL THEN concat('znieff2_id : ', znieff2_id) END AS znieff2,
CASE WHEN appb_nom is NOT NULL THEN concat('appb : ', appb_nom) END AS appb,
CASE WHEN rnn_nom is NOT NULL THEN concat('rnn : ', rnn_nom) END AS rnn,
CASE WHEN rnr_nom is NOT NULL THEN concat('rnr : ', rnr_nom) END AS rnr,
CASE WHEN rb_nom is NOT NULL THEN concat('rnr : ', rb_nom) END AS rb,
--CASE WHEN cen_gest_site is NOT NULL THEN concat('cen : ', cen_gest_site) END AS cen,
CASE WHEN ens_z01_nom is NOT NULL THEN concat('ens_zo1 : ', ens_z01_nom) END AS ens_z01,
CASE WHEN ens_z02_nom is NOT NULL THEN concat('ens_zo2 : ', ens_z02_nom) END AS ens_z02,
CASE WHEN zico_nom is NOT NULL THEN concat('zico : ', zico_nom) END AS zico,
CASE WHEN n2000_sic_nom is NOT NULL THEN concat('n2000_sic : ', n2000_sic_nom) END AS n2000_sic,
CASE WHEN n2000_zps_nom is NOT NULL THEN concat('n2000_zps : ', n2000_zps_nom) END AS n2000_zps,
--CASE WHEN ps_id is NOT NULL THEN concat('ps : ', ps_id) END AS ps38,
--CASE WHEN zh_id is NOT NULL THEN concat('zh : ', zh_id) END AS zh38,
CASE WHEN rncfs_nom is NOT NULL THEN concat('rncfs :', rncfs_nom) END AS rncfs,
CASE WHEN foret_pub_nom is NOT NULL THEN concat('foret_pub :', foret_pub_nom) END AS foret_pub,
--CASE WHEN parcel_foret_pub_nom is NOT NULL THEN concat('parcelle_foret_pub :', parcel_foret_pub_nom) END AS parcel_foret_pub,
geom
FROM com_traitements.communes_intersects_zonages
)a
);