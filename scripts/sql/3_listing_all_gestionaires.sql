DROP TABLE IF EXISTS traitements.listing_gestionnaires_all_cen38;
CREATE TABLE traitements.listing_gestionnaires_all_cen38 as
(
SELECT * FROM
	(
SELECT * FROM traitements.listing_gestionnaires_pln_cen38
UNION ALL
SELECT * FROM traitements.listing_gestionnaires_ppt_cen38
UNION ALL
SELECT * FROM traitements.listing_gestionnaires_psf_cen38
	) a
Group by a.espace, a.id, a.nom, a.gest_site
ORDER BY a.espace
)