DROP TABLE IF EXISTS com_traitements.communes_listing_zonages_cen38;
CREATE TABLE com_traitements.communes_listing_zonages as
(
with
pnr as
(
SELECT 'pnr' as espace ,b.id, b.nom, b.gest_site
FROM communes.ign_communes_38 a, zonages.Parcs_Naturels_regionaux b
WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
GROUP BY b.id, b.nom, b.gest_site
 ),
 pn as
 (
SELECT 'pn' as esapce, c.id, c.nom, c.gest_site
FROM communes.ign_communes_38 a, zonages.Parcs_nationaux c
WHERE a.geom && c.geom AND ST_Intersects(a.geom, c.geom)
GROUP BY c.id, c.nom, c.gest_site
 ),
 znieff2 as
 (
SELECT 'znieff2' as espace, d.id, d.nom, d.gest_site
FROM communes.ign_communes_38 a, zonages.Znieff2 d
WHERE a.geom && d.geom AND ST_Intersects(a.geom, d.geom)
GROUP BY d.id, d.nom, d.gest_site
 ) ,
Znieff1 as
 (
SELECT 'znieff1' as espace,e.id, e.nom, e.gest_site
FROM communes.ign_communes_38 a, zonages.Znieff1 e
WHERE a.geom && e.geom AND ST_Intersects(a.geom, e.geom)
GROUP BY e.id, e.nom, e.gest_site
 ),
appb as
 (
SELECT 'appb' as espace, f.id, f.nom, f.gest_site
FROM communes.ign_communes_38 a, zonages.Arretes_de_protection_de_biotope f
WHERE a.geom && f.geom AND ST_Intersects(a.geom, f.geom)
GROUP BY f.id, f.nom, f.gest_site
 ) ,
rnn as
 (
SELECT 'rnn' as esapce, g.id, g.nom, g.gest_site
FROM communes.ign_communes_38 a, zonages.Reserves_naturelles_nationales g
WHERE a.geom && g.geom AND ST_Intersects(a.geom, g.geom)
GROUP BY g.id, g.nom, g.gest_site
 ),
rnr as
 (
SELECT 'rnr' as espace,h.id, h.nom, h.gest_site
FROM communes.ign_communes_38 a, zonages.Reserves_naturelles_regionales h
WHERE a.geom && h.geom AND ST_Intersects(a.geom, h.geom)
GROUP BY  h.id, h.nom, h.gest_site
 ),
 Reserve_biologique as
 (
SELECT 'Reserve_biologique' as espace,  i.id, i.nom, i.gest_site
FROM communes.ign_communes_38 a, zonages.Reserve_biologique i
WHERE a.geom && i.geom AND ST_Intersects(a.geom, i.geom)	
GROUP BY i.id, i.nom, i.gest_site
 ),
ens_zo1 as
 (
 SELECT 'ens_zo1' as espace, k.id, k.nom, k.gest_site
FROM communes.ign_communes_38 a, zonages.ens_zo_01 k
WHERE a.geom && k.geom AND ST_Intersects(a.geom, k.geom)	
GROUP BY k.id, k.nom, k.gest_site
 ),
 ens_zo2 as
 (
 SELECT 'ens_zo2' as espace, l.id, l.nom, l.gest_site
FROM communes.ign_communes_38 a, zonages.ens_zo_02 l
WHERE a.geom && l.geom AND ST_Intersects(a.geom, l.geom)
GROUP BY l.id, l.nom, l.gest_site
 ),
 zico as
 (
 SELECT 'zico' as espace,m.id, m.nom, m.gest_site
FROM communes.ign_communes_38 a, zonages.zico m
WHERE a.geom && m.geom AND ST_Intersects(a.geom, m.geom)
GROUP BY m.id, m.nom, m.gest_site
 ),
 n2000_sic as
 (
 SELECT 'n2000_sic' as espace, n.id, n.nom, n.gest_site
FROM communes.ign_communes_38 a, zonages.n2000_zps n
WHERE a.geom && n.geom AND ST_Intersects(a.geom, n.geom)
GROUP BY n.id, n.nom, n.gest_site
 ),
 n2000_zps as
 (
 SELECT 'n2000_sic' as espace, o.id, o.nom, o.gest_site
FROM communes.ign_communes_38 a, zonages.n2000_sic o
WHERE a.geom && o.geom AND ST_Intersects(a.geom, o.geom)
GROUP BY o.id, o.nom, o.gest_site
 ),
 zh as
 (
 SELECT 'zh' as espace, p.ogc_fid::varchar(10)  as id , '' as nom, '' as gest_site
FROM communes.ign_communes_38 a, zonages.zones_humides_isere p
WHERE a.geom && p.geom AND ST_Intersects(a.geom, p.geom)
GROUP BY  p.ogc_fid , gest_site
 ),
ps as
 (
 SELECT 'ps' as espace, q.ogc_fid::varchar(10) as id, '' as nom, q.gest_site
FROM communes.ign_communes_38 a, zonages.pelouses_seches q
WHERE a.geom && q.geom AND ST_Intersects(a.geom, q.geom)
GROUP BY q.ogc_fid, q.gest_site
 ),
	onf_forets_publique as
(	
SELECT 'onf_forets_publique' as espace, r.id , r.nom , r.gest_site
FROM communes.ign_communes_38 a, zonages.forets_publique r
WHERE a.geom && r.geom AND ST_Intersects(a.geom, r.geom)
 GROUP BY r.id, r.nom, r.gest_site
),
Parcelles_forets_publique as 		
(	
SELECT 'parcelles_forets_publique' as espace, s.id , s.nom , s.gest_site
FROM communes.ign_communes_38 a, zonages.parcelles_forets_publique s
WHERE a.geom && s.geom AND ST_Intersects(a.geom, s.geom)
 GROUP BY s.id , s.nom , s.gest_site
),
		srce_rb as
		(	
		SELECT 'srce_rb' as espace, v.id_resv as id, '' as nom, '' as gest_site
		FROM communes.ign_communes_38 a, zonages.srce_reservoir_biodiv v
		WHERE a.geom && v.geom AND ST_Intersects(a.geom, v.geom)
		)
	


SELECT * FROM pnr
UNION ALL
SELECT * FROM pn
UNION ALL
SELECT * FROM znieff2
UNION ALL
SELECT * FROM znieff1
UNION ALL
SELECT * FROM appb
UNION ALL
SELECT * FROM rnn
UNION ALL
SELECT * FROM rnr
UNION ALL
SELECT * FROM Reserve_biologique
UNION ALL
SELECT * FROM ens_zo1
UNION ALL
SELECT * FROM ens_zo2
UNION ALL
SELECT * FROM zico
UNION ALL
SELECT * FROM n2000_sic
UNION ALL
SELECT * FROM n2000_zps
--UNION ALL
--SELECT * FROM zh
--UNION ALL
--SELECT * FROM ps
UNION ALL
SELECT * FROM srce_rb
UNION ALL
SELECT * FROM Parcelles_forets_publique
UNION ALL
SELECT * FROM onf_forets_publique
)