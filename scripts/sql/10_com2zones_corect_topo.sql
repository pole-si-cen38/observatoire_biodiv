-- Correction topologique des couches sql
DROP TABLE IF EXISTS zonages.ens_zo_01_corect_topo;
CREATE TABLE zonages.ens_zo_01_corect_topo as
SELECT id,nom, gest_site,
ST_BUFFER(geom, 0.0) as geom
FROM zonages.ens_zo_01;

DROP TABLE IF EXISTS zonages.srce_reservoir_biodiv_corect_topo;
CREATE TABLE zonages.srce_reservoir_biodiv_corect_topo as
SELECT id_resv,
ST_BUFFER(geom, 0.0) as geom
FROM zonages.srce_reservoir_biodiv;

DROP TABLE IF EXISTS zonages.zones_humides_isere_corect_topo;
CREATE TABLE zonages.zones_humides_isere_corect_topo as
SELECT site_code, site_name,
ST_BUFFER(geom, 0.0) as geom
FROM zonages.zones_humides_isere;

DROP TABLE IF EXISTS zonages.pelouses_seches_corect_topo;
CREATE TABLE zonages.pelouses_seches_corect_topo as
SELECT ogc_fid, gest_site,
ST_BUFFER(geom, 0.0) as geom
FROM zonages.pelouses_seches;