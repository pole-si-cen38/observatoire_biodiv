--SELECT ST_SRID(geom) FROM com_traitements.arretes_de_protection_de_biotope LIMIT 1;-- 900914
--SELECT ST_SRID(geom) FROM com_traitements.cen_mu LIMIT 1;--900914
--SELECT ST_SRID(geom) FROM com_traitements.epci LIMIT 1;--900915
--SELECT ST_SRID(geom) FROM com_traitements.n2000_sic LIMIT 1;--900914
--SELECT ST_SRID(geom) FROM com_traitements.zones_humides_isere LIMIT 1;--900914

DROP TABLE IF EXISTS com_traitements.communes_intersects_zonages_aggreg_cen38;
CREATE TABLE com_traitements.communes_intersects_zonages_aggreg_cen38 as
(
with
		pnr as
		(
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.id , ' / ') as id,
						string_agg(b.nom , ' / ') as nom,
						string_agg(b.gest_site , ' / ') as gest_site
				FROM

					(
					SELECT a."a.insee_com",	b.id, b.nom, b.gest_site,
					(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
					FROM communes.ign_communes_38 a, zonages.parcs_naturels_regionaux b
					WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
					AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
					order by percent_recouvrement--a."a.insee_com"
					)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		 ),
		 pn as
		 (
				SELECT	c."a.insee_com" as insee_com,
				string_agg(c.id , ' / ') as id,
							string_agg(c.nom , ' / ') as nom,
							string_agg(c.gest_site , ' / ') as gest_site
				FROM

						(
						SELECT a."a.insee_com",	c.id, c.nom, c.gest_site,
						(st_area(st_intersection(a.geom, c.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.parcs_nationaux c
						WHERE a.geom && c.geom AND ST_Intersects(a.geom, c.geom)
						AND (st_area(st_intersection(a.geom, c.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)c

				GROUP BY c."a.insee_com"
				order by c."a.insee_com"
		 ),
		 znieff2 as
		 (
				SELECT	d."a.insee_com" as insee_com,
						string_agg(d.id , ' / ') as id,
						string_agg(d.nom , ' / ') as nom,
						string_agg(d.gest_site , ' / ') as gest_site
				FROM

						(
						SELECT a."a.insee_com",	d.id, d.nom, d.gest_site,
						(st_area(st_intersection(a.geom, d.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.znieff2 d
						WHERE a.geom && d.geom AND ST_Intersects(a.geom, d.geom)
						AND (st_area(st_intersection(a.geom, d.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)d

				GROUP BY d."a.insee_com"
				order by d."a.insee_com" 
		 ) ,
		Znieff1 as
		 (
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.id , ' / ') as id,
						string_agg(b.nom , ' / ') as nom,
						string_agg(b.gest_site , ' / ') as gest_site
				FROM

						(
						SELECT a."a.insee_com",	b.id, b.nom, b.gest_site,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.znieff1 b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		 ),
		appb as
		 (
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.id , ' / ') as id,
						string_agg(b.nom , ' / ') as nom,
						string_agg(b.gest_site , ' / ') as gest_site
				FROM

						(
						SELECT a."a.insee_com",	b.id, b.nom, b.gest_site,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.arretes_de_protection_de_biotope b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com" 
		 ) ,
		rnn as
		 (
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.id , ' / ') as id,
						string_agg(b.nom , ' / ') as nom,
						string_agg(b.gest_site , ' / ') as gest_site
				FROM

						(
						SELECT a."a.insee_com",	b.id, b.nom, b.gest_site,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.reserves_naturelles_nationales b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"	 
		 ),
		rnr as
		 (
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.id , ' / ') as id,
						string_agg(b.nom , ' / ') as nom,
						string_agg(b.gest_site , ' / ') as gest_site
				FROM

						(
						SELECT a."a.insee_com",	b.id, b.nom, b.gest_site,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.reserves_naturelles_regionales b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"	 
		 ),
		 Reserve_biologique as
		 (
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.id , ' / ') as id,
						string_agg(b.nom , ' / ') as nom,
						string_agg(b.gest_site , ' / ') as gest_site
				FROM

						(
						SELECT a."a.insee_com",	b.id, b.nom, b.gest_site,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.reserve_biologique b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com" 
		 ),
		 ens_zo1 as
		 (
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.id , ' / ') as id,
						string_agg(b.nom , ' / ') as nom,
						string_agg(b.gest_site , ' / ') as gest_site
				FROM

						(
						SELECT a."a.insee_com",	b.id, b.nom, b.gest_site,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.ens_zo_01 b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"	
		 ),
		 ens_zo2 as
		 (
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.id , ' / ') as id,
						string_agg(b.nom , ' / ') as nom,
						string_agg(b.gest_site , ' / ') as gest_site
				FROM

						(
						SELECT a."a.insee_com",	b.id, b.nom, b.gest_site,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.ens_zo_02 b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		 ),
		zico as
		(	
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.id , ' / ') as id,
						string_agg(b.nom , ' / ') as nom,
						string_agg(b.gest_site , ' / ') as gest_site
				FROM

						(
						SELECT a."a.insee_com",	b.id, b.nom, b.gest_site,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.zico b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		),
			n2000_sic as
		(	
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.id , ' / ') as id,
						string_agg(b.nom , ' / ') as nom,
						string_agg(b.gest_site , ' / ') as gest_site
				FROM

						(
						SELECT a."a.insee_com",	b.id, b.nom, b.gest_site,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.n2000_sic b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		),
			n2000_zps as
		(	
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.id , ' / ') as id,
						string_agg(b.nom , ' / ') as nom,
						string_agg(b.gest_site , ' / ') as gest_site
				FROM

						(
						SELECT a."a.insee_com",	b.id, b.nom, b.gest_site,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.n2000_zps b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		),
			zh as
		(	
SELECT	b."a.insee_com" as insee_com,
		string_agg(b.id , ' / ') as id,
		'' as nom,
		'' as gest_site
FROM
		
		(
		SELECT a."a.insee_com",	b.site_code as id,-- b.nom, b.gest_site,
		(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
		FROM communes.ign_communes_38 a, zonages.zones_humides_isere_corect_topo b
		WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
		AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
		order by percent_recouvrement--a."a.insee_com"
		)b
		
GROUP BY b."a.insee_com"
order by b."a.insee_com"
		),
			ps as
		(	
		SELECT	b."a.insee_com" as insee_com,
		string_agg(b."id"::text , ' / ') as id,
		'' as nom,
		'' as gest_site
FROM
		
		(
		SELECT a."a.insee_com",	b.ogc_fid as id,-- b.nom, b.gest_site,
		(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
		FROM communes.ign_communes_38 a, zonages.pelouses_seches_corect_topo b
		WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
		AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
		order by percent_recouvrement--a."a.insee_com"
		)b
		
GROUP BY b."a.insee_com"
order by b."a.insee_com"
		),
			rncfs as
		(	
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.id , ' / ') as id,
						string_agg(b.nom , ' / ') as nom,
						string_agg(b.gest_site , ' / ') as gest_site
				FROM

						(
						SELECT a."a.insee_com",	b.id, b.nom, b.gest_site,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.reserves_nationales_de_chasse_et_faune_sauvage b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		),
		onf_forets_publique as
		(	
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.id , ' / ') as id,
						string_agg(b.nom , ' / ') as nom,
						string_agg(b.gest_site , ' / ') as gest_site
				FROM

						(
						SELECT a."a.insee_com",	b.id, b.nom, b.gest_site,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.forets_publique b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b

				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		),
	
		Parcelles_forets_publique as 		
		(	
SELECT	b."a.insee_com" as insee_com,
		string_agg(b."id"::text , ' / ') as id,
		string_agg(b."nom"::text , ' / ') nom,
		'' as gest_site
FROM
		
		(
		SELECT a."a.insee_com",	b.id as id, b.nom,-- b.nom, b.gest_site,
		(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
		FROM communes.ign_communes_38 a, zonages.parcelles_forets_publique b
		WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
		AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
		order by percent_recouvrement--a."a.insee_com"
		)b
		
GROUP BY b."a.insee_com"
order by b."a.insee_com"
		),
		srce_rb as
		(	
				SELECT	b."a.insee_com" as insee_com,
						string_agg(b.id_resv , ' / ') as id,
						'' as nom,
						'' as gest_site
				FROM
						
						(
						SELECT a."a.insee_com",	b.id_resv,-- b.nom, b.gest_site,
						(st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 as percent_recouvrement
						FROM communes.ign_communes_38 a, zonages.srce_reservoir_biodiv_corect_topo b
						WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
						--AND (st_area(st_intersection(a.geom, b.geom)) / st_area(a.geom))*100 > 1
						order by percent_recouvrement--a."a.insee_com"
						)b
						
				GROUP BY b."a.insee_com"
				order by b."a.insee_com"
		),
	inv_cours_eau_police_eau as
	(
	SELECT	b."a.insee_com" as insee_com,
		string_agg(b."id"::text , ' / ') as id,
		string_agg(b."nom"::text , ' / ') as nom,
		'' as gest_site
FROM
		
		(
		SELECT a."a.insee_com",	b.id_loc as id, b.nom,-- b.nom, b.gest_site,
			st_length(b.geom) as longeur_segment,
		(st_length(st_intersection(a.geom, b.geom)) / st_length(b.geom))*100 as percent_recouvrement
		FROM communes.ign_communes_38 a, zonages.inventaire_cours_eau_police_eau b
		WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
		AND (st_length(st_intersection(a.geom, b.geom)) / st_length(b.geom))*100 > 1
		order by percent_recouvrement--a."a.insee_com"
		)b
		
GROUP BY b."a.insee_com"
order by b."a.insee_com"
	)
	


SELECT
a."a.insee_com" as insee_com,
b.id as pnr_id, b.nom as pnr_nom, -- PNR
c.id as pn_id, c.nom as pn_nom, -- PN
d.id as znieff2_id, d.nom as znieff2_nom, -- znieff2
e.id as znieff1_id, e.nom znieff1_nom, -- znieff1
f.id as appb_id, f.nom as appb_nom, -- appb
g.id as rnn_id, g.nom as rnn_nom , -- rnn
h.id as rnr_id, h.nom as rnr_nom, -- rnr
i.id as rb_id, i.nom as rb_nom, -- Reserve biologique
k.id as ens_z01_id, k.nom as ens_z01_nom, -- ENS_ZO1
l.id as ens_z02_id, l.nom as ens_z02_nom, --ENS_ZO2
m.id as zico_id, m.nom as zico_nom, --ZICO
n.id as n2000_sic_id , n.nom as n2000_sic_nom, --n2000 sic
o.id as n2000_zps_id, o.nom as n2000_zps_nom, -- n2000_zps
p.id as zh_id,-- p.nom as zh_site_nom, -- zh
q.id as ps_id, --q.nom as ps_nom, -- ps
r.id as rncfs_id, r.nom as rncfs_nom, -- RNCFS
t.id as foret_pub_id, t.nom as foret_pub_nom, -- Foret publique
u.id as parcel_foret_pub_id, u.nom as parcel_foret_pub_nom, -- Parcelle foret publique
v.id as srce_reservoir_biodiv, 
w.id as inv_cours_eau_police_eau,
a.geom

FROM communes.ign_communes_38 a
LEFT JOIN pnr b ON a."a.insee_com" = b.insee_com
LEFT JOIN pn c ON a."a.insee_com" = c.insee_com
LEFT JOIN znieff2 d ON a."a.insee_com" = d.insee_com
LEFT JOIN znieff1 e ON a."a.insee_com" = e.insee_com
LEFT JOIN appb f ON a."a.insee_com" = f.insee_com
LEFT JOIN rnn g ON a."a.insee_com" = g.insee_com
LEFT JOIN rnr h ON a."a.insee_com" = h.insee_com
LEFT JOIN Reserve_biologique i ON a."a.insee_com" = i.insee_com
LEFT JOIN ens_zo1 k ON a."a.insee_com" = k.insee_com
LEFT JOIN ens_zo2 l ON a."a.insee_com" = l.insee_com
LEFT JOIN zico m ON a."a.insee_com" = m.insee_com
LEFT JOIN n2000_sic n ON a."a.insee_com" = n.insee_com
LEFT JOIN n2000_zps o ON a."a.insee_com" = o.insee_com
LEFT JOIN zh p ON a."a.insee_com" = p.insee_com
LEFT JOIN ps q ON a."a.insee_com" = q.insee_com
LEFT JOIN rncfs r ON a."a.insee_com" = r.insee_com
LEFT JOIN onf_forets_publique t ON a."a.insee_com" = t.insee_com
LEFT JOIN Parcelles_forets_publique u ON a."a.insee_com" = u.insee_com
LEFT JOIN srce_rb v ON a."a.insee_com" = v.insee_com
LEFT JOIN inv_cours_eau_police_eau w ON a."a.insee_com" = w.insee_com


);