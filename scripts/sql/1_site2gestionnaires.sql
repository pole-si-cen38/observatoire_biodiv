--SELECT ST_SRID(geom) FROM com_traitements.arretes_de_protection_de_biotope LIMIT 1;-- 900914
--SELECT ST_SRID(geom) FROM com_traitements.cen_mu LIMIT 1;--900914
--SELECT ST_SRID(geom) FROM com_traitements.epci LIMIT 1;--900915
--SELECT ST_SRID(geom) FROM com_traitements.n2000_sic LIMIT 1;--900914
--SELECT ST_SRID(geom) FROM com_traitements.zones_humides_isere LIMIT 1;--900914

DROP TABLE IF EXISTS com_traitements.communes_intersects_zonages_cen38;
CREATE TABLE com_traitements.communes_intersects_zonages as
(
with
		pnr as
		(
		SELECT a."a.insee_com", b.*
		FROM communes.ign_communes_38 a, zonages.parcs_naturels_regionaux b
		WHERE a.geom && b.geom AND ST_Intersects(a.geom, b.geom)
		 ),
		 pn as
		 (
		SELECT a."a.insee_com", c.*
		FROM communes.ign_communes_38 a, zonages.parcs_nationaux c
		WHERE a.geom && c.geom AND ST_Intersects(a.geom, c.geom)	 
		 ),
		 znieff2 as
		 (
		SELECT a."a.insee_com", d.*
		FROM communes.ign_communes_38 a, zonages.znieff2 d
		WHERE a.geom && d.geom AND ST_Intersects(a.geom, d.geom)	 
		 ) ,
		Znieff1 as
		 (
		SELECT a."a.insee_com", e.*
		FROM communes.ign_communes_38 a, zonages.znieff1 e
		WHERE a.geom && e.geom AND ST_Intersects(a.geom, e.geom)	 
		 ),
		appb as
		 (
		SELECT a."a.insee_com", f.*
		FROM communes.ign_communes_38 a, zonages.arretes_de_protection_de_biotope f
		WHERE a.geom && f.geom AND ST_Intersects(a.geom, f.geom)	 
		 ) ,
		rnn as
		 (
		SELECT a."a.insee_com", g.*
		FROM communes.ign_communes_38 a, zonages.reserves_naturelles_nationales g
		WHERE a.geom && g.geom AND ST_Intersects(a.geom, g.geom)	 
		 ),
		rnr as
		 (
		SELECT a."a.insee_com", h.*
		FROM communes.ign_communes_38 a, zonages.reserves_naturelles_regionales h
		WHERE a.geom && h.geom AND ST_Intersects(a.geom, h.geom)	 
		 ),
		 Reserve_biologique as
		 (
		SELECT a."a.insee_com", i.*
		FROM communes.ign_communes_38 a, zonages.reserve_biologique i
		WHERE a.geom && i.geom AND ST_Intersects(a.geom, i.geom)	 
		 ),
		 ens_zo1 as
		 (
		 SELECT a."a.insee_com", k.*
		FROM communes.ign_communes_38 a, zonages.ens_zo_01 k
		WHERE a.geom && k.geom AND ST_Intersects(a.geom, k.geom)	
		 ),
		 ens_zo2 as
		 (
		 SELECT a."a.insee_com", l.*
		FROM communes.ign_communes_38 a, zonages.ens_zo_02 l
		WHERE a.geom && l.geom AND ST_Intersects(a.geom, l.geom)	
		 ),
		zico as
		(	
		 SELECT a."a.insee_com", m.*
		FROM communes.ign_communes_38 a, zonages.zico m
		WHERE a.geom && m.geom AND ST_Intersects(a.geom, m.geom)
		),
			n2000_sic as
		(	
		 SELECT a."a.insee_com", n.*
		FROM communes.ign_communes_38 a, zonages.n2000_sic n
		WHERE a.geom && n.geom AND ST_Intersects(a.geom, n.geom)
		),
			n2000_zps as
		(	
		 SELECT a."a.insee_com", o.*
		FROM communes.ign_communes_38 a, zonages.n2000_zps o
		WHERE a.geom && o.geom AND ST_Intersects(a.geom, o.geom)
		),
--			zh as
--		(	
--		 SELECT a."a.insee_com", p.*
--	FROM communes.ign_communes_38 a, zonages.zones_humides_isere p
--		WHERE a.geom && p.geom AND ST_Intersects(a.geom, p.geom)
--		),
--			ps as
--		(	
--		 SELECT a."a.insee_com", q.*
--		FROM communes.ign_communes_38 a, zonages.pelouses_seches q
--		WHERE a.geom && q.geom AND ST_Intersects(a.geom, q.geom)
--		),
			rncfs as
		(	
		 SELECT a."a.insee_com", r.*
		FROM communes.ign_communes_38 a, zonages.reserves_nationales_de_chasse_et_faune_sauvage r
		WHERE a.geom && r.geom AND ST_Intersects(a.geom, r.geom)
		),
		onf_forets_publique as
		(	
		SELECT a."a.insee_com", t.*
		FROM communes.ign_communes_38 a, zonages.forets_publique t
		WHERE a.geom && t.geom AND ST_Intersects(a.geom, t.geom)
		),
	
--		Parcelles_forets_publique as 		
--		(	
--		SELECT a."a.insee_com", u.*
--		FROM communes.ign_communes_38 a, zonages.parcelles_forets_publique u
--		WHERE a.geom && u.geom AND ST_Intersects(a.geom, u.geom)
--		)
		srce_rb as
		(	
		SELECT a."a.insee_com", v.*
		FROM communes.ign_communes_38 a, zonages.srce_reservoir_biodiv v
		WHERE a.geom && v.geom AND ST_Intersects(a.geom, v.geom)
		)
	


SELECT
a."a.insee_com",
b.id as pnr_id, b.nom as pnr_nom, -- PNR
c.id as pn_id, c.nom as pn_nom, -- PN
d.id as znieff2_id, d.nom as znieff2_nom, -- znieff2
e.id as znieff1_id, e.nom znieff1_nom, -- znieff1
f.id as appb_id, f.nom as appb_nom, -- appb
g.id as rnn_id, g.nom as rnn_nom , -- rnn
h.id as rnr_id, h.nom as rnr_nom, -- rnr
i.id as rb_id, i.nom as rb_nom, -- Reserve biologique
k.id as ens_z01_id, k.nom as ens_z01_nom, -- ENS_ZO1
l.id as ens_z02_id, l.nom as ens_z02_nom, --ENS_ZO2
m.id as zico_id, m.nom as zico_nom, --ZICO
n.id as n2000_sic_id , n.nom as n2000_sic_nom, --n2000 sic
o.id as n2000_zps_id, o.nom as n2000_zps_nom, -- n2000_zps
--p.ogc_fid as zh_id,-- p.nom as zh_site_nom, -- zh
--q.ogc_fid as ps_id, --q.nom as ps_nom, -- ps
r.id as rncfs_id, r.nom as rncfs_nom, -- RNCFS
t.id as foret_pub_id, t.nom as foret_pub_nom, -- Foret publique
--u.id as parcel_foret_pub_id, u.nom as parcel_foret_pub_nom, -- Parcelle foret publique
v.id_resv as srce_reservoir_biodiv, 
a.geom

FROM communes.ign_communes_38 a
LEFT JOIN pnr b ON a."a.insee_com" = b."a.insee_com"
LEFT JOIN pn c ON a."a.insee_com" = c."a.insee_com"
LEFT JOIN znieff2 d ON a."a.insee_com" = d."a.insee_com"
LEFT JOIN znieff1 e ON a."a.insee_com" = e."a.insee_com"
LEFT JOIN appb f ON a."a.insee_com" = f."a.insee_com"
LEFT JOIN rnn g ON a."a.insee_com" = g."a.insee_com"
LEFT JOIN rnr h ON a."a.insee_com" = h."a.insee_com"
LEFT JOIN Reserve_biologique i ON a."a.insee_com" = i."a.insee_com"
LEFT JOIN ens_zo1 k ON a."a.insee_com" = k."a.insee_com"
LEFT JOIN ens_zo2 l ON a."a.insee_com" = l."a.insee_com"
LEFT JOIN zico m ON a."a.insee_com" = m."a.insee_com"
LEFT JOIN n2000_sic n ON a."a.insee_com" = n."a.insee_com"
LEFT JOIN n2000_zps o ON a."a.insee_com" = o."a.insee_com"
--LEFT JOIN zh p ON a."a.insee_com" = p."a.insee_com"
--LEFT JOIN ps q ON a."a.insee_com" = q."a.insee_com"
LEFT JOIN rncfs r ON a."a.insee_com" = r."a.insee_com"
LEFT JOIN onf_forets_publique t ON a."a.insee_com" = t."a.insee_com"
--LEFT JOIN Parcelles_forets_publique u ON a."a.insee_com" = u."a.insee_com"
LEFT JOIN srce_rb v ON a."a.insee_com" = v."a.insee_com"

);