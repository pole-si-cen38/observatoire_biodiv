create table com_traitements.synthese_com_peu_zonage as
SELECT insee_com, pnr, pn, znieff2, znieff1, appb,
reserves_naturelles_nationales, reserves_naturelles_regionales,
reserve_biologique, ens_zo_01, ens_zo_02, zico,
n2000_sic, n2000_zps, zones_humides, pelouses_seches, reserves_nationales_de_chasse_et_faune_sauvage, forets_publique, parcelles_forets_publique, srce_reservoir_biodiv, inventaire_cours_eau_police_eau, geom
	FROM com_traitements.communes_intersects_zonages_aggreg_saut_ligne_merge_field_cen38
where
pnr is null and -- 86
pn is null and -- 13
znieff1 is null and -- 392
znieff2 is null and -- 386
appb is null and --47
reserves_naturelles_nationales is null and --22
reserves_naturelles_regionales is null and --11
reserve_biologique is null and -- 5
ens_zo_01 is null and -- 107
ens_zo_02 is null and -- 55
zico is null and --51
n2000_sic is null and --109
n2000_zps is null and --19
zones_humides is null and-- 322 communes
pelouses_seches is null and-- 83 communes
reserves_nationales_de_chasse_et_faune_sauvage is null and -- 2
forets_publique is null and -- 243
parcelles_forets_publique is null and -- and -- 176
srce_reservoir_biodiv is null -- 448
-- inventaire_cours_eau_police_eau is not null --509